# import, Set device...

PATH = 'data/...'
sz = 224
arch = resnext101_64
bs=58  # 64, si riduce se out of memory

label_csv = f'{PATH}label_csv'
n = len(list(open(label_csv))) - 1
val_idxs = get_cv_idxs(n)  # random 20% rows


get_data(sz,bs):
	tfms = tfms_from_model(arch, sz, aug_tfms=transforms_side_on, max_zoom=1.1)
	data = ImageClassifierData.from_csv(PATH, 'train', label_csv, tfms=tfms,
	                    suffix='.jpg', val_idxs=val_idxs, test_name='test', bs=bs)  # attenzione: suffix non ci va
	if sz < 300:
		data = data.resize(340, 'tmp')  # ???
	return data


#img = PIL.Image.open(filename); img





learn = ConvLearner.pretrained(arch, data, precomputed=True)

learn.fit(1e-2, 5)


learn.precompute = False


learn.fit(1e-2, 5, cycle_len=1)  # cycle = epoch


learn.save('224_pre')
learn.load('224_pre')


learn.set_data(get_data(299, bs))  # continua con immagini più grandi - trick di jeremy
learn.freeze()  # just to be sure


learn.fit(1e-2, 3, cycle_len=1)
# noto che è underfitting, validation loss è molto più basso -- DA CAPIRE
learn.fit(1e-2, 3, cycle_len=1, cycle_mult=2)

#
log_preds, y = learn.TTA()
probs = np.exp(log_preds)
accuracy(log_preds, y), metrics.log_loss(y, probs)
learn.save('299_pre')
learn.load('299_pre')


learn.fit(1e-2, 1, cycle_len=2) # Gli diamo ancora una botta

# eventualmente provare unfreeze()
