#!/usr/bin/env python

import sys
from collections import defaultdict

unique_values_by_column = defaultdict(set)
csv_files = ['kpis_1998_2003.csv',
             'kpis_2004_2008.csv',
             'kpis_2009_2011.csv',
             'kpis_2012_2013.csv']

d = unique_values_by_column
# for csv in csv_files:
#     print 'Loading file "{}"'.format(csv)
#     with open(csv, 'r') as f:
#         headers = None
#         debug_cnt = 0
#         for line in f:
#             debug_cnt += 1
#             if debug_cnt % 5000 == 0:
#                 sys.stdout.write('.')
#             if debug_cnt % (5000*50) == 0:
#                 sys.stdout.write('\n')
#             # if debug_cnt == 200:
#             #     break
#             values = [v.strip() for v in line.split(',')]
#             if len(values) == 0:
#                 continue
#             if headers is None:
#                 headers = values
#             else:
#                 for i, v in enumerate(values):
#                     if i >= len(headers):
#                         continue  # skip last field - it is text, hard to parse
#
#                     d[headers[i]].add(v)
#                     if headers[i] == 'period' and v == 'period':
#                         print '*** Unexpected {}'.format(values)
#         print '\nCompleted'

print d['period']
print d['municipality_id']


def find_headers_duplicates():
    with open(csv_files[0], 'r') as f:
        first_line = None
        for i, line in enumerate(f):
            if len(line) > 0:
                if first_line is None:
                    first_line = line
                else:
                    if line == first_line:
                        print 'Line no. {} is a duplicate of the header line'.format(i)

find_headers_duplicates()