#!/usr/bin/python


def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error).
    """
    print 'outlierCleaner'

    cleaned_data = [(age, net_worth, (pred - net_worth) ** 2) for age, net_worth, pred in zip(ages, net_worths, predictions)]
    cleaned_data = sorted(cleaned_data, key=lambda tup: tup[2]) # sort by error ascending
    # print cleaned_data[0]
    # print cleaned_data[-1]
    cleaned_data = cleaned_data[:81]

    
    return cleaned_data

