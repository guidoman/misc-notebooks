#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle
import math

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))

# print len(enron_data["SKILLING JEFFREY K"])
# poi  = {k, v for k, v in enron_data if v.poi is True}
print 'Total = {}'.format(len(enron_data))
poi = {k:enron_data[k] for k in enron_data.keys() if enron_data[k]['poi'] is True}
print 'Number of POI = {}'.format(len(poi))
no_poi = {k:enron_data[k] for k in enron_data.keys() if enron_data[k]['poi'] is False}
print 'Number of No POI = {}'.format(len(no_poi))

# print enron_data['PRENTICE JAMES']['total_stock_value']
# print enron_data['COLWELL WESLEY']['from_this_person_to_poi']
# print enron_data['MORAN MICHAEL P']['total_payments']

def find_people_with_no_payments(data):
    return [k for k in data.keys() if math.isnan(float(data[k]['total_payments']))]



nan_payments = find_people_with_no_payments(enron_data)
print 'Number of persons with no payment = {}'.format(len(nan_payments))

poi_nan_payments = find_people_with_no_payments(poi)
print 'Number of POI with no payment = {}'.format(len(poi_nan_payments))


