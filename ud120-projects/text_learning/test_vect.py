from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.sparse

word_data = [	'car car buy',
				'buy car automobile',
				'may be book incorrect for texaco buy',
				'susan samantha and stephani on the email is is',
				'susan email buy the on is',
				'veronica subject buy car',
				'please reply samantha veronica is the',
				'send the forego list book susan email',
				'car automobile susan car veronica',
				'guido car book incorrect texaco buy'
				]
labels = [0, 0, 1, 1, 1, 0, 1, 0, 0, 0]

assert len(word_data) == len(labels)


# vectorizer

# vectorizer = CountVectorizer(stop_words='english')
vectorizer = TfidfVectorizer(stop_words='english')
# t = vectorizer.fit_transform(word_data)

# split train / test
from sklearn import cross_validation
features_train, features_test, labels_train, labels_test = cross_validation.train_test_split(word_data,
	labels, test_size=0.2, random_state=42)

# print(features_train)
# print(features_test)
# print(labels_train)
# print(labels_test)

# Transform text

# ???
# vectorizer.fit(word_data)
# features_train = vectorizer.transform(features_train)
# ---?
features_train = vectorizer.fit_transform(features_train)
# ---?
features_test  = vectorizer.transform(features_test)
print(len(vectorizer.get_feature_names()))
print(features_train)
print('***')
# print(features_test)

# FEATURE SELECTION
# selector = SelectPercentile(f_classif, percentile=10)
# selector.fit(features_train_transformed, labels_train)
# features_train_transformed = selector.transform(features_train_transformed).toarray()
# features_test_transformed  = selector.transform(features_test_transformed).toarray()

# Apply classifier
from sklearn import tree
clf = tree.DecisionTreeClassifier()
clf = clf.fit(features_train.toarray(), labels_train)
accuracy = clf.score(features_test.toarray(), labels_test)
print('Accuracy = {}'.format(accuracy))







