#!/usr/bin/python

import matplotlib.pyplot as plt
from prep_terrain_data import makeTerrainData
from class_vis import prettyPicture
from sklearn import tree
from sklearn import neighbors, datasets
from sklearn.ensemble import RandomForestClassifier


features_train, labels_train, features_test, labels_test = makeTerrainData()


### the training data (features_train, labels_train) have both "fast" and "slow"
### points mixed together--separate them so we can give them different colors
### in the scatterplot and identify them visually
grade_fast = [features_train[ii][0] for ii in range(0, len(features_train)) if labels_train[ii]==0]
bumpy_fast = [features_train[ii][1] for ii in range(0, len(features_train)) if labels_train[ii]==0]
grade_slow = [features_train[ii][0] for ii in range(0, len(features_train)) if labels_train[ii]==1]
bumpy_slow = [features_train[ii][1] for ii in range(0, len(features_train)) if labels_train[ii]==1]



# clf = tree.DecisionTreeClassifier(min_samples_split=40)
n_neighbors = 5
clf = RandomForestClassifier(n_estimators=10)
# clf = neighbors.KNeighborsClassifier(n_neighbors, weights='uniform')
clf.fit(features_train, labels_train)
accuracy = clf.score(features_test, labels_test)
print 'Accuracy = {}'.format(accuracy)


prettyPicture(clf, features_test, labels_test)


try:
    prettyPicture(clf, features_test, labels_test)
except NameError:
    print NameError
