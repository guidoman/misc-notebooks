#!/usr/bin/python


"""
    Starter code for the evaluation mini-project.
    Start by copying your trained/tested POI identifier from
    that which you built in the validation mini-project.

    This is the second step toward building your POI identifier!

    Start by loading/formatting the data...
"""

import pickle
import sys
import numpy as np
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn.metrics import precision_score, recall_score

data_dict = pickle.load(open("../final_project/final_project_dataset.pkl", "r") )

### add more features to features_list!
features_list = ["poi", "salary"]

data = featureFormat(data_dict, features_list)
labels, features = targetFeatureSplit(data)



### your code goes here

def get_true_positives_indexes(actual_labels, predicted_labels):
	return [ii for ii, (actual, predicted) in enumerate(zip(actual_labels, predicted_labels)) if predicted != 0.0 and actual != 0.0]

X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.3, random_state=42)

clf = tree.DecisionTreeClassifier()
clf.fit(X_train, y_train)
pred = clf.predict(X_test)
print 'Number of predicted POIs = {}'.format(np.count_nonzero(pred))
print 'Total number of people = {}'.format(len(pred))
accuracy = clf.score(X_test, y_test)
print 'Accuracy = {}'.format(accuracy)
print 'Number of true positives for normal tree = {}'.format(len(get_true_positives_indexes(y_test, pred)))

# overfit tree
overfitClf = tree.DecisionTreeClassifier()
overfitClf.fit(features, labels)
overfitPred = overfitClf.predict(features)
accuracy = overfitClf.score(features, labels)
print 'Overfit accuracy = {}'.format(accuracy)
print 'Number of true positives for overfit tree = {}'.format(len(get_true_positives_indexes(labels, overfitPred)))

# Metrics
print 'Precision score = {}'.format(precision_score(y_test, pred))
print 'Recall score = {}'.format(recall_score(y_test, pred))


# Quiz
quiz_predictions = [0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1] 
quiz_true_labels = [0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0]
true_positives_cnt, true_negatives_cnt, false_positives_cnt, false_negatives_cnt = 0, 0, 0, 0
for p, l in zip(quiz_predictions, quiz_true_labels):
	if p == 1 and l == 1:
		true_positives_cnt += 1
	elif p == 1 and l == 0:
		false_positives_cnt += 1
	elif p == 0 and l == 1:
		false_negatives_cnt += 1
	else:
		true_negatives_cnt += 1

print (true_positives_cnt, true_negatives_cnt, false_positives_cnt, false_negatives_cnt)
print 'Quiz Precision score = {}'.format(precision_score(quiz_true_labels, quiz_predictions))
print 'Quiz Recall score = {}'.format(recall_score(quiz_true_labels, quiz_predictions))


