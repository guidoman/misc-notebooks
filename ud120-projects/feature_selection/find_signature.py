#!/usr/bin/python

import pickle
import numpy
numpy.random.seed(42)
from sklearn import tree


### The words (features) and authors (labels), already largely processed.
### These files should have been created from the previous (Lesson 10)
### mini-project.
words_file = "../text_learning/your_word_data.pkl" 
authors_file = "../text_learning/your_email_authors.pkl"
word_data = pickle.load( open(words_file, "r"))
authors = pickle.load( open(authors_file, "r") )



### test_size is the percentage of events assigned to the test set (the
### remainder go into training)
### feature matrices changed to dense representations for compatibility with
### classifier functions in versions 0.15.2 and earlier
from sklearn import cross_validation
features_train, features_test, labels_train, labels_test = cross_validation.train_test_split(word_data, authors, test_size=0.1, random_state=42)

from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
                             stop_words='english')
features_train = vectorizer.fit_transform(features_train)
print 'Features names training = {}'.format(len(vectorizer.get_feature_names()))
features_test  = vectorizer.transform(features_test).toarray()
print 'Features names testing = {}'.format(len(vectorizer.get_feature_names()))
print 'Features train original size = {}'.format(len(features_train.toarray()))
print 'Features test original size = {}'.format(len(features_test))


# print 'features_train before split = {}'.format(len(features_train))
# print 'features_test before split = {}'.format(len(features_test))


### a classic way to overfit is to use a small number
### of data points and a large number of features;
### train on only 150 events to put ourselves in this regime
features_train = features_train[:150].toarray()
labels_train   = labels_train[:150]
print 'Features train original size = {}'.format(len(features_train))




### your code goes here


clf = tree.DecisionTreeClassifier()
clf = clf.fit(features_train, labels_train)
accuracy = clf.score(features_test, labels_test)
print 'Decision tree accuracy = {}'.format(accuracy)
# print 'Number of features = {}'.format(len(clf.feature_importances_))
maxIndex = clf.feature_importances_.argmax()
print 'Max feature index = {}'.format(maxIndex)
print 'Max feature value = {}'.format(clf.feature_importances_[maxIndex])
# print 'N features = {}'.format(clf.n_features_)
# print 'N features test = {}'.format(len(features_test))
# print clf.feature_importances_[:10]
feature_names = vectorizer.get_feature_names()
print 'Max feature name = {}'.format(feature_names[maxIndex])

# Checking Important Features Again
threshold = 0.2
important_features = [feature_names[index] for index, importance in enumerate(clf.feature_importances_) if importance > threshold]
print 'Number of "important features" = {}'.format(len(important_features))
print 'Important features:'
for f in important_features:
	print f
